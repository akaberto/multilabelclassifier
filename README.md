# Introduction

This project's aim is to build a multi-label classifier for board
games.  Since board games are quite complex and their genres may not
be well defined or they may fall into multiple buckets, it makes
monetary sense to make a classifier for this and see where all it
should fall into.

## Members

-   Gwen
-   Prashant
-   Shubham
-   Akash

## Structure of code

-   ./Root
    -   ./raw\_data
        -   All the data shall be stored here.  The original table shall
            be here.  This shall not be mutated.
    -   ./processed
        -   Any processed data shall be stored here.  Processing shall
            also be carried out here.  Used for storing intermediate data
            here.
    -   ./src
        -   Store code here.
        -   ./python
            -   Store python code here.
        -   ./r
            -   Store R code here.
        -   ./matlab 
            -   Store matlab code here.
    -   ./resources
        -   Store papers here.
    -   ./misc 
        -   The things that can't be classified.  Talk about irony.

from sklearn.preprocessing import *
import pandas as pd
import numpy as np
from sklearn.multiclass import OneVsRestClassifier
from scipy import *
from collections import *
from sklearn.preprocessing import OneHotEncoder
from sklearn.svm import LinearSVC
from sklearn.preprocessing import *
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from scipy.optimize import *

#load dataset
def load_data():
	data=pd.read_csv("Data.csv").as_matrix()

	#selecting columns of interest which have the one-hot encoing of the labels
	y=data[:,20:2961]
	#Features of the data
	x=data[:,0:19]

	#LabelEncoding
	x=pd.DataFrame(x)
	d=x.apply(LabelEncoder().fit_transform)
	d=d.as_matrix()
	return d,y

#Splitting the dataset into training and testing dataset
d,y=load_data()
X_train, X_test, y_train, y_test = train_test_split(d,y,test_size=0.1, random_state=42)

#Collective Multi Label Algorithm
#First set of constraints for a single sample 
def constraints_1_each(x,y): 
	f_x_y=[]
	for l in range():
		for j in range(2942):
			f_x_y.append(x[l]*(y[j]==1))

	return np.asarray(f_x_y)

#Second set of constraints
def constraints_2_each(y):
	f_x_y=[]
	for j1 in range(2941):
		for j2 in range(j1+1,2942):
			f_x_y.append((y[j1]==1)*(y[j2]==1))

	return np.asarray(f_x_y)

#_lambda is a np.zeros([2,1]) which has two lambdas corrosponding to two classes
#Substituting lambda here will give p(x|y) 
# Z function
def summation(_lambda):
	(x,y)=load_data()
	ans=0.0; sum1=0.0; sum2=0.0
	f_x_y_1=[]
	f_x_y_2=[]
	for j in range(2942):
		f_x_y_1.append(x[i]*(y[j]==1))
	for j1 in range(2941):
		for j2 in range(j1+1,2942):
			f_x_y_2.append((y[j1]==1)*(y[j2]==1))
	sum1=sum(f_x_y_1)*_lambda[0]
	f_x_y_2=constraints_2_each(y)
	sum2=sum(f_x_y_2)*_lambda[1]
	return sum1+sum2

def p_x_y(_lambda):
	(x,y)=load_data()
	f_x_y_1=constraints_1_each(x,y)
	f_x_y_2=constraints_2_each(y)
	sum1=_lambda[0]*f_x_y_1
	sum2=_lambda[1]*f_x_y_2
	return -np.exp(sum(sum1+sum2))

#This is Z, this surely needs to be changed 
def summation(_lambda):
	(x,y)=load_data()
	ans=0.0; sum1=0.0; sum2=0.0
	f_x_y_1=[]
	f_x_y_2=[]
	for j in range(2942):
		f_x_y_1.append(x[i]*(y[j]==1))
	for j1 in range(2941):
		for j2 in range(j1+1,2942):
			f_x_y_2.append((y[j1]==1)*(y[j2]==1))
	sum1=sum(f_x_y_1)*_lambda[0]
	f_x_y_2=constraints_2_each(y)
	sum2=sum(f_x_y_2)*_lambda[1]
	return sum1+sum2

#Z(xd) for each sample xd
def normalize_Z(_lambda):
	a=summation(_lambda)
	return np.exp(a)

#log likelihood function which needs to be optimised and how to give in 
# variance which is required by the function, see var in the function
def log_func_optimize(_lambda):
	a=summation(_lambda)
	b=0
	for i in range(len(X_train)):
		b+=np.log(normalize_Z(i,_lambda))
	s=a-b-((_lambda[0]**2)-(_lambda[1]**2)/float(2*var**2))
	return s

#Gradient for Optimization, need to add the p(x|y) component here
#need to figure that out
def func_optimize_der(_lambda):
	(x,y)=load_data()
	ans=0.0
	for i in range(x.shape[0]):
		f_x_y_1=constraints_1_each(x[i],y)
		sum1=sum(f_x_y_1)
		f_x_y_2=constraints_2_each(y)
		sum2=sum(f_x_y_2)

#x0 is the test input, will have to change accordingly
def accuracy():
	count=0
	for i in range(X_test.shape[0]):
		res = fmin_bfgs(log_func_optimize,X_test[i],jac=func_optimize_der)
		#load_data has to be changed in such a way that the split data 
		#(with trainig and test data) is obtained as well 
		#Calculates the probability
		d=p_x_y(res)
		if (np.array_equal(d,y_test[i])):
			count+=1
	return count/len(X_test)

#Basic outline of the algo:
#1) Calculate the constraints 1 and 2 which I am calculating in 
#   constraints_1_each and constraints_2_each
#2) put back the value of lambdas obtained from the optimization (BFGS)
# 	to find the value of p(x|y)
#4) figure out how to find Z-partition function
#3) couldnt figure out the max Px|y thing here and how will that com into the picture
 
from sklearn.preprocessing import *
import pandas as pd
import numpy as np
from sklearn.multiclass import OneVsRestClassifier
from scipy import *
from collections import *
from sklearn.preprocessing import OneHotEncoder
from sklearn.svm import LinearSVC
from sklearn.preprocessing import *
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from scipy.optimize import *
from itertools import combinations



#load dataset
def load_data(split_ratio):
	data=pd.read_csv("Data.csv").as_matrix()

	#selecting columns of interest which have the one-hot encoing of the labels
	y=data[:,20:2961]
	#Features of the data
	x=data[:,0:19]

	#LabelEncoding
	x=pd.DataFrame(x)
	d=x.apply(LabelEncoder().fit_transform)
	d=d.as_matrix()
	X_train, X_test, y_train, y_test = train_test_split(d,y,test_size=split_ratio, random_state=42)
	
	return X_train,y_train,X_test,y_test

#Splitting the dataset into training and testing dataset
X_train, X_test, y_train, y_test = load_data()

#Collective Multi Label Algorithm
#First set of constraints for a single sample 
def constraint_1(x,y): 
	return np.kron(x,y)

#Second set of constraints
#Second set of constraints
def constraints_2_each(y):
	A = list(combinations(y,2))
	y = map(lambda (x,y): x*y, A)
	return np.toarray(y)



#_lambda is a np.zeros([2,1]) which has two lambdas corrosponding to two classes
#Substituting lambda here will give p(x|y) 
# Z function
#l1 and l2 have the same length as f_x_y1 and f_x_y2
def summation(x,y,l1,l2):
	fxy1 = constraints_1_each(x,y)
	fxy2 = constraints_2_each(y)	
	a = l1.dot(fxy1)
	b = l2.dot(fxy2)
	return np.exp(a + b)

#Y is the collection of all Y label vectors
def Z_xd(x, y, l1, l2):
	ans=summation(x,ele,l1,l2)
	return ans

def p_x_y(l1,l2,x,y):
	f_x_y_1=constraints_1_each(x,y)
	f_x_y_2=constraints_2_each(y)
	sum1 = l1.dot(f_x_y_1)
	sum2 = l2.dot(f_x_y_2)
	return -np.exp(sum(sum1+sum2))/Z_xd(x,y,l1,l2)

#log likelihood function which needs to be optimised and how to give in 
# variance which is required by the function, see var in the function
#inputs go in matrix form
#variance is in e^2 form
def log_func_optimize(L, X, Y, var):
	a=0; b=0
	m,n = X[1,:].shape 
	l1=L[0:m*n]
	l2=L[m*n+1:end]
	for x,y in zip(X,Y):
		a+=np.log(summation(x,y,l1,l2))
		b+=np.log(Z_xd(x,y,l1,l2))
	l_1=map(lambda x,y: (x**2 + y**2)/2*var, l1, l2)
	return a + b -l_1

#Gradient for Optimization, need to add the p(x|y) component here
#need to figure that out
def func_optimize_der_each(L, X, Y, var,k):
	#(x,y)=load_data(0.1)
	#Lets assume that we have a constraint vector
	sum1=0
	if k<=20*2941:
		for i in range(X.shape[0]):
			for i in range
			sum1+=


		

#x0 is the test input, will have to change accordingly
def accuracy():
	count=0
	for i in range(X_test.shape[0]):
		res = fmin_bfgs(log_func_optimize,X_test[i],jac=func_optimize_der)
		#load_data has to be changed in such a way that the split data 
		#(with trainig and test data) is obtained as well 
		#Calculates the probability
		d=p_x_y(res)
		if (np.array_equal(d,y_test[i])):
			count+=1
	return count/len(X_test)

#Basic outline of the algo:
#1) Calculate the constraints 1 and 2 which I am calculating in 
#   constraints_1_each and constraints_2_each
#2) put back the value of lambdas obtained from the optimization (BFGS)
# 	to find the value of p(x|y)
#4) figure out how to find Z-partition function
#3) couldnt figure out the max Px|y thing here and how will that com into the picture
 
# concatenate the constraints into a single list and then use

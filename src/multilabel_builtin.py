from skmultilearn.problem_transform import LabelPowerset
from skmultilearn.problem_transform import ClassifierChain
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.metrics import hamming_loss
from sklearn.preprocessing import LabelEncoder 
from sklearn.metrics import hamming_loss,zero_one_loss,label_ranking_loss,average_precision_score,f1_score,accuracy_score
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score,precision_score
from skmultilearn.ensemble import RakelO
from skmultilearn.ensemble import LabelSpacePartitioningClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score,precision_recall_curve
from sklearn.metrics import roc_auc_score,recall_score,confusion_matrix
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.ensemble import RandomForestClassifier
#from sklearn.multioutput import ClassifierChain

data=pd.read_csv("Data.csv",sep=',')

input_col = ["rank","names","min_players","max_players","avg_time","min_time","max_time","avg_rating","geek_rating","num_votes","age","category","weight"]

mechanics = ["Action Point Allowance System","Co-operative Play","Hand Management","Point to Point Movement","Set Collection","Trading","Variable Player Powers","Auction/Bidding","Card Drafting","Area Control / Area Influence","Campaign / Battle Card Driven","Dice Rolling","Simultaneous Action Selection","Route/Network Building","Variable Phase Order","Action / Movement Programming","Grid Movement","Modular Board","Storytelling","Area Movement","Tile Placement","Worker Placement","Deck / Pool Building","Role Playing","Memory-mechanic","Partnerships","Pick-up and Deliver","Player Elimination","Secret Unit Deployment","Pattern Recognition","Press Your Luck","Time Track","Voting","Area-Impulse","Hex-and-Counter","Area Enclosure","Pattern Building","Take That","Stock Holding","Commodity Speculation","Simulation","Betting/Wagering","Trick-taking","Line Drawing","Rock-Paper-Scissors","Roll / Spin and Move","Paper-and-Pencil","Acting","Singing","none-mechanic","Chit-Pull System","Crayon Rail System"]

category = ["Environmental","Medical","Card Game","Civilization","Economic","Modern Warfare","Political","Wargame","Fantasy","Territory Building","Adventure","Exploration","Fighting","Miniatures","Dice","Movies / TV / Radio theme","Science Fiction","Industry / Manufacturing","Ancient","City Building","Animals","Farming","Medieval","Novel-based","Mythology","American West","Horror","Murder/Mystery","Puzzle","Video Game Theme","Space Exploration","Collectible Components","Bluffing","Transportation","Religious","Travel","Nautical","Deduction","Party Game","Spies/Secret Agents","Word Game","Mature / Adult","Renaissance","Zombies","Negotiation","Abstract Strategy","Prehistoric","Arabian","Aviation / Flight","Post-Napoleonic","Trains","Action / Dexterity","World War I","World War II","Comic Book / Strip","Racing","Real-time","Humor","Electronic","Book","Civil War","Expansion for Base-game","Sports","Pirates","Age of Reason","none-category","American Indian Wars","American Revolutionary War","Educational","Memory-category","Maze","Napoleonic","Print & Play","American Civil War","Children's Game","Vietnam War","Pike and Shot","Mafia","Trivia","Number","Game System","Korean War","Music","Math"]

x = data[input_col+mechanics]
y = data[category]
d=x.apply(LabelEncoder().fit_transform)
d=d.as_matrix()
X_train, X_test, y_train, y_test = train_test_split(d,y,test_size=0.05, random_state=42)

#print y_train.shape
# X_train,y_train,X_test,y_test=load_data(0.2)
# newinst=RakelO(classifier=GaussianNB(),model_count=20,labelset_size=10)
# newinst.generate_partition(X_train,y_train)
# newinst.classifiers=[GaussianNB()]
# newinst.fit(X_train,y_train) 
n=np.arange(84)
np.random.seed(42)
np.random.shuffle(n)
print n
clf=LabelPowerset(GaussianNB())
clf_cc = ClassifierChain(RandomForestClassifier(n_estimators=100,max_depth=200))
#clf_cc1= ClassifierChain(GaussianNB())
# def exact_match(x,y):
# 	count=0
# 	m,n=y.shape
# 	for i in range(m):
# 		for j in range(n):
# 			if(x[i][j]==y[i][j]):
# 				count+=1
# 	return count/float((m*n))

#fit classifier
clf.fit(X_train,y_train.astype(float))
clf_cc.fit(X_train,y_train.astype(float))
#predictions
predictions = clf.predict(X_test)
predictions_cc = clf_cc.predict(X_test)
pred_prob = clf_cc.predict_proba(X_test)
# y_score = clf_cc.decision_function(X_test)
print type(predictions_cc)

# precision=dict()
# recall=dict()
# average_precision=dict()
# for i in range(y_test.shape[1]):
# 	precision[i], recall[i], _ = precision_recall_curve(y_test[:, i],
#                                                         y_score[:, i])
# 	average_precision[i] = average_precision_score(y_test[:, i], y_score[:, i])

# m=MultiLabelBinarizer().fit(y_test)

#f=f1_score(m.transform(y_test),m.transform(predictions_cc),average='macro')
# precision["micro"], recall["micro"], _ = precision_recall_curve(y_test.ravel(),y_score.ravel())
# average_precision["micro"] = average_precision_score(Y_test,y_score,average="micro")
# print('Average precision score, micro-averaged over all classes: {0:0.2f}'.format(average_precision["micro"]))
# print "Classification report: \n", (classification_report(y_test, predictions_cc))
# print "F1 micro averaging:",(f1_score(y_test, predictions_cc, average='micro'))
#print "ROC: ",(roc_auc_score(y_test, predictions_cc.todense())
r1=recall_score(y_true=y_test, y_pred=predictions_cc, average='micro')
r2=recall_score(y_true=y_test, y_pred=predictions_cc, average='macro')
p1=precision_score(y_true=y_test, y_pred=predictions_cc, average='micro')
p2=precision_score(y_true=y_test, y_pred=predictions_cc, average='macro')
f1=f1_score(y_true=y_test, y_pred=predictions_cc, average='micro')
f2=f1_score(y_true=y_test, y_pred=predictions_cc, average='macro')


# # # "bgg_url","game_id", "year","image_url","owned", "desi
Score_cc_ham=hamming_loss(y_test,predictions_cc)
# Score_cc_zero=precision_score(y_test,predictions_cc)
#c=confusion_matrix(y_test,predictions_cc.toarray())
print "Hamming Loss for classifier chains", Score_cc_ham
# print "zero_one_loss for CC", Score_cc_zero
print "The micro recall is", r1
print "The macro recall is", r2
print "The micro precision is", p1
print "The macro precision is", p2
print "The micro F-1 score is", f1
print "The macro F-1 score is", f2
print r1,r2,p1,p2,f1,f2